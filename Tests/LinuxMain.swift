import XCTest

import NCCEnvironmentTricksTests

var tests = [XCTestCaseEntry]()
tests += NCCEnvironmentTricksTests.allTests()
XCTMain(tests)
