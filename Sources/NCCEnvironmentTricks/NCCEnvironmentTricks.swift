// NCCEnvironmentTricks.swift
// Copyright (C) 2020 Nuclear Cyborg Corp
//
// Writtem by Andrew Benson, db@nuclearcyborg.com

import Foundation

public struct NCCEnvironmentTricks {

    /// True if this process has a debugger attached by a debugger
    static public var isDebuggerAttached: Bool {
        var info = kinfo_proc()
        var mib : [Int32] = [CTL_KERN, KERN_PROC, KERN_PROC_PID, getpid()]
        var size = MemoryLayout<kinfo_proc>.stride
        let result = sysctl(&mib, UInt32(mib.count), &info, &size, nil, 0)
        assert(result == 0, "sysctl failed")
        return (info.kp_proc.p_flag & P_TRACED) != 0
    }
}
